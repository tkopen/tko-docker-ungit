FROM node:12.7.0-alpine

RUN apk add --no-cache \
        git \
        net-tools \
        openssh
    
RUN npm install ungit@1.5.24 -g

USER node
WORKDIR /home/node

VOLUME ["/var/www"]

EXPOSE 8448

CMD ["ungit", "--no-launchBrowser", "--forcedLaunchPath=/var/www"]
